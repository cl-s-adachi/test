import sys
import pprint
import pymongo
import itertools
from bson import ObjectId
import pandas as pd
import logging
logging.basicConfig(filename='logfile/logger.log', level=logging.DEBUG)
client = pymongo.MongoClient('mongodb://mlops:mlops123!@mlops.cluster-cnywsjyajpyh.us-east-1.docdb.amazonaws.com:27017/?ssl=true&ssl_ca_certs=rds-combined-ca-bundle.pem&replicaSet=rs0&readPreference=secondaryPreferred')
documents_ids=[]
evaluation_documents=[]

def get_data_mongo(db,source_collection_name,source_ids,desire_source_field):
   target_ids=[]
   for source_id in source_ids:
      collection_name=db[source_collection_name]
      source_data=collection_name.find({"_id":ObjectId(source_id)})
      target_ids_df= pd.DataFrame(list(source_data))
      if desire_source_field == 'AnnotationIds' :
         s=(target_ids_df[desire_source_field].values)
         target_ids.extend(list(itertools.chain.from_iterable(s)))
      else :
         target_ids.extend(target_ids_df[desire_source_field].values.tolist())
   return target_ids

def insert_once(db,collection_name,document):
      collection=db[collection_name]
      result=collection.insert_one(document)
      result=str(result.inserted_id)
      remove_sql(result,collection_name)
      return result

def insert_many(db,collection_name,documents):
      result_ids=[]
      collection=db[collection_name]
      result=collection.insert_many(documents)
      for id in result.inserted_ids:
         result_ids.extend(str(id)) 
      for id in result_ids:
         remove_sql(id,collection_name)

def remove_sql(id,collection):
      logging.info("db."+collection+".find({\"_id\":ObjectId(\""+id+"\")})")
      logging.info("db."+collection+".remove({\"_id\":ObjectId(\""+id+"\")})")
     

#####################################################################
#####Collectionを表示する
#####################################################################
def showCollection(db,collection_name,document1,document2):
    collection_name=db[collection_name]
    ids=collection_name.find(document1,document2)
    for id in ids:
       print(id)
def showCollection_raw(db,collection_name):
    collection_name=db[collection_name]
    ids=collection_name.find()
    for id in ids:
       print(id)

print("#------------------#")
for database_name in client.list_database_names():
        print("use "+database_name)
print("#------------------#")
db_name = input('Enter DB name: ')
db=client[db_name]
#------------------
#db = client['dataproc-test']
showCollection(db,"DataSet",{"DataSetType" : "TEST"},{"ReleaseVersion":0,"AnnotationTarget":0,"CheckStatus":0})
dataset_test_id = input('Test用のDataSetを入力してください: ')
showCollection(db,"DataSet",{"DataSetType" : "TRAIN"},{"ReleaseVersion":0,"AnnotationTarget":0,"CheckStatus":0})
dataset_train_id= input('Train用のDataSetを入力してください: ')



#dataset_test_id="5ebe2219d1d25f810ebff80b"
#dataset_train_id="5ebe4a74526cc0bc97bfe687"
#dataset_train_ids=dataset_train_id.split()
#dataset_test_ids=dataset_test_id.split()
#annotation_train_ids=get_data_mongo(db,"DataSet",dataset_train_ids,"AnnotationIds")
#image_train_ids=get_data_mongo(db,"annotation",annotation_train_ids,"ImageId")
#annotation_test_ids=get_data_mongo(db,"DataSet",dataset_test_ids,"AnnotationIds")
#image_test_ids=get_data_mongo(db,"annotation",annotation_test_ids,"ImageId")

#####################################################################
#####CodeExecutionにPreProcessedサンプルデータをインサートする。
#####################################################################
processed_flag=input('新しくProcessedDataSetを作成する場合は1,既存のProcesseDatasetを利用する場合は2を入力してください:')
if processed_flag == "1" :
   code_excecution_preprosessing_id=insert_once(db,"CodeExecution",{"CodeType":"PREPROCESSING","DockerImagePath":"latest","DockerFilePath":"DockerImage/yyy/dammy_xxx_image.tar","HardwareInfoPath":"Info/hardware/hard_xxxx_file","HostOSInfoPath":"Info/os/os_xxx_file","ExternalSoftwareInfoPath":"Info/ex/software_xxx_file","OtherInputInfoPath":"Info/other/other_xxx_file","SourceCodePath":"xxxyyy"})


#####################################################################
####PreprocessedDataSetにサンプルデータをインサートする。
#####################################################################
   preprocessed_data_set_train_id=insert_once(db,"PreprocessedDataSet",{"DataSetId":dataset_train_id,"CodeExecutionId":code_excecution_preprosessing_id,"SelectionReasonPath":"SelectionReason/xxx/srfile2","TrainDataSet":"data/train/train1.tfrecord","TestDataSetPath":"data/test/test1.tfrecord"})
   preprocessed_data_set_test_id=insert_once(db,"PreprocessedDataSet",{"DataSetId":dataset_test_id,"CodeExecutionId":code_excecution_preprosessing_id,"SelectionReasonPath":"SelectionReason/xxx/srfile2","TrainDataSet":"data/train/train1.tfrecord","TestDataSetPath":"data/test/test1.tfrecord"})
else:
   showCollection_raw(db,"annotation")
   preprocessed_data_set_test_id=input("DataSetType TESTにつけるPreprosessedIDを入力してください")
   preprocessed_data_set_train_id=input("DataSetType TRAINにつけるPreprossed IDを入力してください")

#####################################################################
##a##CodeExecutionnにTRANINGサンプルデータをインサートする。
#####################################################################
code_excecution_training_id=insert_once(db,"CodeExecution", {"CodeType" : "TRAINING", "DockerImagePath" : "latest", "DockerFilePath" : "DockerImage/TRAINING/traning_xxx_image.tar", "HardwareInfoPath" : "Info/hardware/TRAINING/hard_xxxx_file", "HostOSInfoPath" : "Info/os/os_xxx_file", "ExternalSoftwareInfoPath" : "Info/ex/software_xxxx_file", "OtherInputInfoPath" : "Info/other/other_xxxx_file", "SourceCodePath" : "xxxyyy" })


#####################################################################
####TrainingJobにサンプルデータをインサートする。
#####################################################################
training_job_id=insert_once(db,"TrainingJob", {"TrainingType" : "FullScratch", "PreprocessedDataSetId" : preprocessed_data_set_test_id, "Foreign_TrainingJobId" : "null", "CodeExecutionId" : code_excecution_training_id, "TrainedModelPath" : "model/yyy/model_xxx.tar.gz" })

#####################################################################
##a##CodeExecutionにINFERENCEサンプルデータをインサートする。
#####################################################################
code_excecution_inference_id=insert_once(db,"CodeExecution",{"CodeType" : "INFERENCE", "DockerImagePath" : "latest", "DockerFilePath" : "DockerImage/INFERENCE/inference_xxxx_image1.tar", "HardwareInfoPath" : "Info/hardware/INFERENCE/hard_xxx_file", "HostOSInfoPath" : "Info/os/os_xxx_file", "ExternalSoftwareInfoPath" : "Info/ex/software_xxx_file", "OtherInputInfoPath" : "Info/other/other_xxx_file", "SourceCodePath" : "xxxyyy" })


#####################################################################
####InferenceJobにサンプルデータをインサートする
#####################################################################
inference_job_id=insert_once(db,"InferenceJob",{"TrainingJobId" : training_job_id, "PreprocessedDataSetId" : preprocessed_data_set_train_id, "CodeExecutionId" : code_excecution_inference_id })

#####################################################################
####InferenceJobにサンプルデータをインサートする
#####################################################################
for image_id in image_train_ids:
   insert_once(db,"Inference",{"ImageId" : image_id, "InferencePath" : "InferenceOutput/xxxx/inference_output_yyy_file", "InferenceJobId" : inference_job_id })

#####################################################################
##a##CodeExecutionにEvaluationサンプルデータをインサートする。
#####################################################################
code_excecution_evaluation_id=insert_once(db,"CodeExecution",{"CodeType" : "EVALUATION", "DockerImagePath" : "latest", "DockerFilePath" : "DockerImage/EVALUATION/evaluation_xxx_image.tar", "HardwareInfoPath" : "Info/hardware/EVALUATION/hard_xxx_file1e", "HostOSInfoPath" : "Info/os/os_xxx_file", "ExternalSoftwareInfoPath" : "Info/ex/software_xxx_file", "OtherInputInfoPath" : "Info/other/other_xxx_file", "SourceCodePath" : "xxxyyy" })

#####################################################################
####EvalationJobにサンプルデータをインサートする
#####################################################################
for image_id in image_train_ids :
   evaluation_documents+=[{"InferenceJobId" : inference_job_id, "CodeExecutionId" : code_excecution_evaluation_id, "EvaluationResultPath" : "EvaluationResult/yyy/evaluation_xxx_file", "EvaluationValue" : [ { "metrixA" : 0.9, "metrixB" : 0.7 } ] }]

   #insert_once(db,"EvaluationJob",{"InferenceJobId" : inference_job_id, "CodeExecutionId" : code_excecution_evaluation_id, "EvaluationResultPath" : "EvaluationResult/yyy/evaluation_xxx_file", "EvaluationValue" : [ { "metrixA" : 0.9, "metrixB" : 0.7 } ] })

insert_many(db,"EvaluationJob",evaluation_documents)

